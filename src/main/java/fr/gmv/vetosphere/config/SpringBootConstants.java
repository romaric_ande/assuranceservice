package fr.gmv.vetosphere.config;

public class SpringBootConstants {

    public static String SPRING_PROFILE_DEVELOPMENT = "dev";

    public static String SPRING_PROFILE_TEST = "test";

    public static String SPRING_PROFILE_PRODUCTION = "prod";

    public static String SPRING_PROFILE_CLOUD = "cloud";

    public static String SPRING_PROFILE_SWAGGER = "swagger";

    public static String SPRING_PROFILE_NO_LIQUIBASE = "no-liquibase";

}
