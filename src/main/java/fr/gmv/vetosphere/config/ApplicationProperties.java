package fr.gmv.vetosphere.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to VetosphereStarter application
 * <p>
 * Properties are configured in the {@code application.yml} file.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
}
