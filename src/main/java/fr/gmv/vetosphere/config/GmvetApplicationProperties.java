package fr.gmv.vetosphere.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@ConfigurationProperties(prefix = "gmvet", ignoreUnknownFields = false)
@Validated
public class GmvetApplicationProperties {

    private final GmvetUrl vet1 = new GmvetUrl();
    private final GmvetUrl vet2 = new GmvetUrl();

    public GmvetUrl getVet1() {
        return vet1;
    }

    public GmvetUrl getVet2() {
        return vet2;
    }

    public static class GmvetUrl {

        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}