package fr.gmv.vetosphere.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.gmv.vetosphere.domain.Assurance;
import fr.gmv.vetosphere.domain.Teletransmission;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

public class LiaisonDto implements Serializable {

    private Long id;
    private String authKey;
    private Long assuranceId;
    private Long siteId;
    private Boolean isDeleted;
    private Long teletransmissions;
    private Long assurance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public Long getAssuranceId() {
        return assuranceId;
    }

    public void setAssuranceId(Long assuranceId) {
        this.assuranceId = assuranceId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Long getTeletransmissions() {
        return teletransmissions;
    }

    public void setTeletransmissions(Long teletransmissions) {
        this.teletransmissions = teletransmissions;
    }

    public Long getAssurance() {
        return assurance;
    }

    public void setAssurance(Long assurance) {
        this.assurance = assurance;
    }
}
