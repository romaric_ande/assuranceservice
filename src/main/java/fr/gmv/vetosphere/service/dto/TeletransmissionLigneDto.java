package fr.gmv.vetosphere.service.dto;

import fr.gmv.vetosphere.domain.Teletransmission;
import java.io.Serializable;

public class TeletransmissionLigneDto implements Serializable {

    private Long id;
    private Long ligneId;
    private String gmvPlateform;
    private Boolean isDeleted;
    private Long teletransmission;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLigneId() {
        return ligneId;
    }

    public void setLigneId(Long ligneId) {
        this.ligneId = ligneId;
    }

    public String getGmvPlateform() {
        return gmvPlateform;
    }

    public void setGmvPlateform(String gmvPlateform) {
        this.gmvPlateform = gmvPlateform;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Long getTeletransmission() {
        return teletransmission;
    }

    public void setTeletransmission(Long teletransmission) {
        this.teletransmission = teletransmission;
    }
}
