package fr.gmv.vetosphere.service.dto;

import fr.gmv.vetosphere.domain.Assurance;
import fr.gmv.vetosphere.domain.Liaison;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class AssuranceDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String urlApi;
    private Boolean isDeleted;
    private Boolean supporteTeletransmission;
    private Long liaisons;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlApi() {
        return urlApi;
    }

    public void setUrlApi(String urlApi) {
        this.urlApi = urlApi;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Boolean getSupporteTeletransmission() {
        return supporteTeletransmission;
    }

    public void setSupporteTeletransmission(Boolean supporteTeletransmission) {
        this.supporteTeletransmission = supporteTeletransmission;
    }

    public Long getLiaisons() {
        return liaisons;
    }

    public void setLiaisons(Long liaisons) {
        this.liaisons = liaisons;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssuranceDto that = (AssuranceDto) o;
        return Objects.equals(id, that.id) &&
                name.equals(that.name) &&
                Objects.equals(urlApi, that.urlApi) &&
                Objects.equals(isDeleted, that.isDeleted) &&
                Objects.equals(supporteTeletransmission, that.supporteTeletransmission) &&
                liaisons.equals(that.liaisons);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, urlApi, isDeleted, supporteTeletransmission, liaisons);
    }
}
