package fr.gmv.vetosphere.service.dto;

import fr.gmv.vetosphere.domain.Liaison;
import fr.gmv.vetosphere.domain.TeletransmissionLigne;
import java.io.Serializable;
import java.time.Instant;
import java.util.Set;

public class TeletransmissionDto implements Serializable {

    private Long id;
    private Instant date = Instant.now();
    private String action;
    private String numeroPolice;
    private Boolean success;
    private Long status;
    private String message;
    private String response;
    private Boolean isDeleted;
    private Long teletransmissionLignes;
    private Long liaison;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getNumeroPolice() {
        return numeroPolice;
    }

    public void setNumeroPolice(String numeroPolice) {
        this.numeroPolice = numeroPolice;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Long getTeletransmissionLignes() {
        return teletransmissionLignes;
    }

    public void setTeletransmissionLignes(Long teletransmissionLignes) {
        this.teletransmissionLignes = teletransmissionLignes;
    }

    public Long getLiaison() {
        return liaison;
    }

    public void setLiaison(Long liaison) {
        this.liaison = liaison;
    }
}
