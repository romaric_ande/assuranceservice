package fr.gmv.vetosphere.service;

import fr.gmv.vetosphere.service.dto.AssuranceDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;


public interface AssuranceService {


    /**
     * Save a assurance.
     *
     * @param assuranceDto the entity to save
     * @return the persisted entity
     */
    AssuranceDto save(AssuranceDto assuranceDto);

    /**
     * Get all the assurance.
     *
     * @param pageable the pagination information
     * @param id account id
     * @return the list of entities
     */
    Page<AssuranceDto> findAll(Pageable pageable, Long id);

    /**
     * Get the "id" assurance.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AssuranceDto> findOne(Long id);

    /**
     * Delete the "id" assurance.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

}
