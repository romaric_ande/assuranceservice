package fr.gmv.vetosphere.web.rest.api;

import com.codahale.metrics.annotation.Timed;
import fr.gmv.vetosphere.domain.Assurance;
import fr.gmv.vetosphere.repository.AssuranceRepository;
import fr.gmv.vetosphere.security.AuthoritiesConstants;
import fr.gmv.vetosphere.service.AssuranceService;
//import fr.gmv.vetosphere.web.rest.util.PaginationUtil;
import fr.gmv.vetosphere.service.dto.AssuranceDto;
import fr.gmv.vetosphere.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class AssuranceResource {

    private static final Logger log = LoggerFactory.getLogger(AssuranceResource.class);
    public static final String ENTITY_NAME = "assurance";
    private final MessageSource messageSource;


    @Autowired
    private AssuranceRepository assuranceRepository;

    private AssuranceService assuranceService;

    public AssuranceResource(MessageSource messageSource) {
        this.messageSource = messageSource;
//        this.assuranceService = assuranceService;
    }

    /**
     * GET /assurances : get all the assurance.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of agenda in
     * body
     * @throws URISyntaxException if there is an error to generate the pagination
     *                            HTTP headers
     */
    @GetMapping("/assurances")
    @Secured({AuthoritiesConstants.ADMIN})
    @Timed
    public ResponseEntity<List<AssuranceDto>> getAllAgenda(@PageableDefault(size = 100) Pageable pageable,
                                                           @RequestParam(value = "id", required = false) Long id) {
        log.debug("REST request to get a page of Assurance");
        List<Assurance> page = assuranceRepository.getAllAssurance();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
