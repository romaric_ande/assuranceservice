package fr.gmv.vetosphere.web.rest.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.FieldError;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation<T> implements Serializable {

    private static final long serialVersionUID = 3858129921828536031L;

    public static final String REGEXP_COLOR = "#[0-9a-fA-F]{2}[0-9a-fA-F]{2}[0-9a-fA-F]{2}";
    public static final Pattern PATTERN_COLOR = Pattern.compile(REGEXP_COLOR);

    public static final String PARAM_VALUE = "value";
    public static final String PARAM_MIN_VALUE = "min";
    public static final String PARAM_MAX_VALUE = "max";
    public static final String PARAM_PATTERN = "pattern";

    public static final String MSG_REQUIRED = "{validation.required}";
    public static final String MSG_RANGE = "{validation.range}";
    public static final String MSG_LENGTH = "{validation.length}";
    public static final String MSG_MIN_SIZE = "{validation.minSize}";
    public static final String MSG_MAX_SIZE = "{validation.maxSize}";
    public static final String MSG_MIN = "{validation.min}";
    public static final String MSG_MAX = "{validation.max}";
    public static final String MSG_PATTERN = "{validation.pattern}";

    public static final String FIELD_GENERAL = "GENERAL";

    private transient MessageSource messageSource;
    private transient Class<T> clazz;
    private List<FieldError> errors;

    public Validation(MessageSource messageSource, Class<T> clazz) {
        this.messageSource = messageSource;
        this.clazz = clazz;
        this.errors = new ArrayList<>();
    }

    public List<FieldError> getErrors() {
        return errors;
    }

    public Validation<T> required(String field, String value) {
        if (StringUtils.isBlank(value)) {
            Map<String, String> params = new HashMap<>();
            params.put(PARAM_VALUE, value);
            this.errors.add(new FieldError(getObjectName(), field, getFormattedMsg(MSG_REQUIRED, params)));
        }
        return this;
    }

    public Validation<T> required(String field, Integer i) {
        String value;
        value = (i == null)?"":i.toString();
        return required(field, value);
    }

    public Validation<T> minSize(String field, int value, int minValue) {
        if (value < minValue) {
            Map<String, String> params = new HashMap<>();
            params.put(PARAM_VALUE, "" + value);
            params.put(PARAM_MIN_VALUE, "" + minValue);
            this.errors.add(new FieldError(getObjectName(), field, getFormattedMsg(MSG_MIN_SIZE, params)));
        }
        return this;
    }

    public Validation<T> maxSize(String field, int value, int maxValue) {
        if (value > maxValue) {
            Map<String, String> params = new HashMap<>();
            params.put(PARAM_VALUE, "" + value);
            params.put(PARAM_MAX_VALUE, "" + maxValue);
            this.errors.add(new FieldError(getObjectName(), field, getFormattedMsg(MSG_MAX_SIZE, params)));
        }
        return this;
    }

    public Validation<T> minSize(String field, String str, int minValue) {
        int value = 0;
        if (str != null) {
            value = str.length();
        }
        return minSize(field, value, minValue);
    }

    public Validation<T> maxSize(String field, String str, int maxValue) {
        int value = 0;
        if (str != null) {
            value = str.length();
        }
        return maxSize(field, value, maxValue);
    }

    public Validation<T> minSize(String field, Object[] array, int minValue) {
        int value = 0;
        if (array != null) {
            value = array.length;
        }
        return minSize(field, value, minValue);
    }

    public Validation<T> maxSize(String field, Object[] array, int maxValue) {
        int value = 0;
        if (array != null) {
            value = array.length;
        }
        return maxSize(field, value, maxValue);
    }

    public Validation<T> min(String field, int value, int minValue) {
        if (value < minValue) {
            Map<String, String> params = new HashMap<>();
            params.put(PARAM_VALUE, "" + value);
            params.put(PARAM_MIN_VALUE, "" + minValue);
            this.errors.add(new FieldError(getObjectName(), field, getFormattedMsg(MSG_MIN, params)));
        }
        return this;
    }

    public Validation<T> max(String field, int value, int maxValue) {
        if (value > maxValue) {
            Map<String, String> params = new HashMap<>();
            params.put(PARAM_VALUE, "" + value);
            params.put(PARAM_MAX_VALUE, "" + maxValue);
            this.errors.add(new FieldError(getObjectName(), field, getFormattedMsg(MSG_MAX, params)));
        }
        return this;
    }

    public Validation<T> color(String field, String value) {
        if (value == null) {
            value = "";
        }
        Pattern pattern = PATTERN_COLOR;
        Matcher m = pattern.matcher(value);
        if (!m.matches()) {
            Map<String, String> params = new HashMap<>();
            params.put(PARAM_VALUE, value);
            params.put(PARAM_PATTERN, pattern.pattern());
            this.errors.add(new FieldError(getObjectName(), field, getFormattedMsg(MSG_PATTERN, params)));
        }
        return this;
    }

    public Validation<T> range(String field, int value, int minValue, int maxValue) {
        if ((value < minValue) || (value > maxValue)) {
            Map<String, String> params = new HashMap<>();
            params.put(PARAM_VALUE, "" + value);
            params.put(PARAM_MIN_VALUE, "" + minValue);
            params.put(PARAM_MAX_VALUE, "" + maxValue);
            this.errors.add(new FieldError(getObjectName(), field, getFormattedMsg(MSG_RANGE, params)));
        }
        return this;
    }


    public Validation<T> length(String field, String value, int min, int max) {
        if ((value != null)
                && ((value.length() < min) || (value.length() > max))) {
            Map<String, String> params = new HashMap<>();
            params.put(PARAM_VALUE, value);
            params.put(PARAM_MIN_VALUE, "" + min);
            params.put(PARAM_MAX_VALUE, "" + max);
            this.errors.add(new FieldError(getObjectName(), field, getFormattedMsg(MSG_LENGTH, params)));
        }
        return this;
    }

    public Validation<T> customError(String field, String messageKey) {
        this.errors.add(new FieldError(getObjectName(), field,  messageSource.getMessage(messageKey,null,messageKey,null)));
        return this;
    }

    public boolean hasErrors() {
        return (errors != null) && (!errors.isEmpty());
    }

    public String getObjectName() {
        return (clazz==null)?"":clazz.getSimpleName();
    }

    public String getFormattedMsg(String msgKey, Map<String, String> params) {
        String msg = getMsg(msgKey);
        for (Entry<String, String> entry : params.entrySet()) {
            msg = msg.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
        }
        return msg;
    }

    public String getMsg(String msgKey) {
        if (msgKey == null) return "";

        while (msgKey.startsWith("{")) {
            msgKey = msgKey.substring(1);
        }
        while (msgKey.endsWith("}")) {
            msgKey = msgKey.substring(0, msgKey.length() - 1);
        }

        return messageSource.getMessage(msgKey, null, "", null);
    }

}
