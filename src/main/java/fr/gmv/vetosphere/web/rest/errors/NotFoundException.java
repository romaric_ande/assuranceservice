package fr.gmv.vetosphere.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

/**
 * ResourceNotFouncException
 */
public class NotFoundException extends AbstractThrowableProblem {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundException(String message) {
        super(null, message, Status.NOT_FOUND);
	}
	
	public NotFoundException() {
        super(null,"Resource not found", Status.NOT_FOUND);
    }
}