package fr.gmv.vetosphere.web.rest.errors;


public final class ErrorConstants {

    private ErrorConstants() {
    }

    public static final String ERROR_MESSAGE = "error.missing_params";

}


