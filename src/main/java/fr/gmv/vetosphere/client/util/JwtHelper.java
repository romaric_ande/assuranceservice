package fr.gmv.vetosphere.client.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import java.util.Base64;

public class JwtHelper {

    private static Logger logger = LoggerFactory.getLogger(JwtHelper.class);

    private String headersPart;
    private String payloadPart;
    private String signaturePart;

    public JwtHelper(String token) {
        if (token != null) {
            String[] parts = token.split("\\.");
            if (parts.length == 3) {
                headersPart = parts[0];
                payloadPart = parts[1];
                signaturePart = parts[2];
            }
        }
    }

    public JSONObject getPayload() {
        if (payloadPart != null) {
            try {
                return new JSONObject(new String(Base64.getDecoder().decode(payloadPart)));
            } catch (JSONException err) {
                logger.debug("Erreur lors de decodage du jwt");
            }
        }
        return null;
    }

    public JSONObject getHeaders() {
        if (headersPart != null) {
            try {
                return new JSONObject(new String(Base64.getDecoder().decode(headersPart)));
            } catch (JSONException err) {
                logger.debug("Erreur lors de decodage du jwt");
            }
        }
        return null;
    }

}
