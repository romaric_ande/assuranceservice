package fr.gmv.vetosphere.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "teletransmission")
public class Teletransmission extends AbstractAuditingEntity{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private Instant date = Instant.now();

    @Column(name = "action", length = 5)
    private String action;

    @Column(name = "numero_police", length = 255)
    private String numeroPolice;

    @Column(name = "success")
    private Boolean success;

    @Column(name = "status")
    private Long status;

    @Column(name = "message", length = 255)
    private String message;

    @Column(name = "response", length = 255)
    private String response;

    @Column(name = "isDeleted")
    private Boolean isDeleted;

    @OneToMany(mappedBy = "teletransmission", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonIgnore
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TeletransmissionLigne> teletransmissionLignes;

    @ManyToOne
    @JoinColumn(name = "liaison_id", nullable = false)
    private Liaison liaison;

    public Teletransmission() {
    }

    public Teletransmission(Instant date, String action, String numeroPolice, Boolean success, Long status,
                            String message, String response, Long liaisonId, Boolean isDeleted) {
        this.date = date;
        this.action = action;
        this.numeroPolice = numeroPolice;
        this.success = success;
        this.status = status;
        this.message = message;
        this.response = response;
        this.isDeleted = isDeleted;
    }

    public Teletransmission(Instant date, String action, String numeroPolice, Boolean success, Long status,
                            String message, String response, Boolean isDeleted, Set<TeletransmissionLigne> teletransmissionLignes,
                            Liaison liaison) {
        this.date = date;
        this.action = action;
        this.numeroPolice = numeroPolice;
        this.success = success;
        this.status = status;
        this.message = message;
        this.response = response;
        this.isDeleted = isDeleted;
        this.teletransmissionLignes = teletransmissionLignes;
        this.liaison = liaison;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getNumeroPolice() {
        return numeroPolice;
    }

    public void setNumeroPolice(String numeroPolice) {
        this.numeroPolice = numeroPolice;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Set<TeletransmissionLigne> getTeletransmissionLignes() {
        return teletransmissionLignes;
    }

    public void setTeletransmissionLignes(Set<TeletransmissionLigne> teletransmissionLignes) {
        this.teletransmissionLignes = teletransmissionLignes;
    }

    public Liaison getLiaison() {
        return liaison;
    }

    public void setLiaison(Liaison liaison) {
        this.liaison = liaison;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teletransmission that = (Teletransmission) o;
        return Objects.equals(id, that.id) &&
                date.equals(that.date) &&
                Objects.equals(action, that.action) &&
                numeroPolice.equals(that.numeroPolice) &&
                Objects.equals(success, that.success) &&
                Objects.equals(status, that.status) &&
                Objects.equals(message, that.message) &&
                Objects.equals(response, that.response) &&
                Objects.equals(isDeleted, that.isDeleted) &&
                Objects.equals(teletransmissionLignes, that.teletransmissionLignes) &&
                Objects.equals(liaison, that.liaison);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, action, numeroPolice, success, status, message, response, isDeleted, teletransmissionLignes, liaison);
    }

    @Override
    public String toString() {
        return "Teletransmission{" +
                "id=" + id +
                ", date=" + date +
                ", action='" + action + '\'' +
                ", numeroPolice='" + numeroPolice + '\'' +
                ", success=" + success +
                ", status=" + status +
                ", message='" + message + '\'' +
                ", response='" + response + '\'' +
                ", isDeleted=" + isDeleted +
                ", teletransmissionLignes=" + teletransmissionLignes +
                ", liaison=" + liaison +
                '}';
    }
}
