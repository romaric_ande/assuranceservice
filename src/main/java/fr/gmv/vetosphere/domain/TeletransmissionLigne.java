package fr.gmv.vetosphere.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "teletransmission_ligne")
public class TeletransmissionLigne extends AbstractAuditingEntity{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ligne_id")
    private Long ligneId;

    @Column(name = "gmv_plateform")
    private String gmvPlateform;

    @Column(name = "isDeleted")
    private Boolean isDeleted;

    @ManyToOne
    @JoinColumn(name = "teletransmission_id", nullable = false)
    private Teletransmission teletransmission;

    public TeletransmissionLigne() {}

    public TeletransmissionLigne(Long ligneId, String gmvPlateform, Boolean isDeleted) {
        this.ligneId = ligneId;
        this.gmvPlateform = gmvPlateform;
        this.isDeleted = isDeleted;
    }

    public TeletransmissionLigne(Long ligneId, String gmvPlateform, Boolean isDeleted, Teletransmission teletransmission) {
        this.ligneId = ligneId;
        this.gmvPlateform = gmvPlateform;
        this.isDeleted = isDeleted;
        this.teletransmission = teletransmission;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLigneId() {
        return ligneId;
    }

    public void setLigneId(Long ligneId) {
        this.ligneId = ligneId;
    }

    public String getGmvPlateform() {
        return gmvPlateform;
    }

    public void setGmvPlateform(String gmvPlateform) {
        this.gmvPlateform = gmvPlateform;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Teletransmission getTeletransmission() {
        return teletransmission;
    }

    public void setTeletransmission(Teletransmission teletransmission) {
        this.teletransmission = teletransmission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeletransmissionLigne that = (TeletransmissionLigne) o;
        return Objects.equals(id, that.id) &&
                ligneId.equals(that.ligneId) &&
                gmvPlateform.equals(that.gmvPlateform) &&
                Objects.equals(isDeleted, that.isDeleted) &&
                Objects.equals(teletransmission, that.teletransmission);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ligneId, gmvPlateform, isDeleted, teletransmission);
    }

    @Override
    public String toString() {
        return "TeletransmissionLigne{" +
                "id=" + id +
                ", ligneId=" + ligneId +
                ", gmvPlateform='" + gmvPlateform + '\'' +
                ", isDeleted=" + isDeleted +
                ", teletransmission=" + teletransmission +
                '}';
    }
}
