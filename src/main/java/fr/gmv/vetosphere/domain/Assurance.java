package fr.gmv.vetosphere.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import javax.persistence.*;
import java.util.Objects;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "assurance")
public class Assurance extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @Length(min = 3, max = 255)
    private String name;

    @Column(name = "url_api")
    @Length(min = 3, max = 255)
    private String urlApi;

    @Column(name = "isDeleted")
    private Boolean isDeleted;

    @Column(name = "supporte_teletransmission")
    private Boolean supporteTeletransmission;
    
    @OneToMany(mappedBy = "assurance", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Liaison> liaisons;

    public Assurance() {
    }

    public Assurance(@Length(min = 3, max = 255) String name, @Length(min = 3, max = 255) String urlApi,
                     Boolean isDeleted, Boolean supporteTeletransmission) {
        this.name = name;
        this.urlApi = urlApi;
        this.isDeleted = isDeleted;
        this.supporteTeletransmission = supporteTeletransmission;
    }

    public Assurance(@Length(min = 3, max = 255) String name, @Length(min = 3, max = 255) String urlApi,
                     Boolean isDeleted, Boolean supporteTeletransmission, Set<Liaison> liaisons) {
        this.name = name;
        this.urlApi = urlApi;
        this.isDeleted = isDeleted;
        this.supporteTeletransmission = supporteTeletransmission;
        this.liaisons = liaisons;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlApi() {
        return urlApi;
    }

    public void setUrlApi(String urlApi) {
        this.urlApi = urlApi;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Boolean getSupporteTeletransmission() {
        return supporteTeletransmission;
    }

    public void setSupporteTeletransmission(Boolean supporteTeletransmission) {
        this.supporteTeletransmission = supporteTeletransmission;
    }

    public Set<Liaison> getLiaisons() {
        return liaisons;
    }

    public void setLiaisons(Set<Liaison> liaisons) {
        this.liaisons = liaisons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Assurance assurance = (Assurance) o;
        return Objects.equals(id, assurance.id) &&
                name.equals(assurance.name) &&
                Objects.equals(urlApi, assurance.urlApi) &&
                Objects.equals(isDeleted, assurance.isDeleted) &&
                Objects.equals(supporteTeletransmission, assurance.supporteTeletransmission) &&
                Objects.equals(liaisons, assurance.liaisons);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, urlApi, isDeleted, supporteTeletransmission, liaisons);
    }

    @Override
    public String toString() {
        return "Assurance{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", urlApi='" + urlApi + '\'' +
                ", isDeleted=" + isDeleted +
                ", supporteTeletransmission=" + supporteTeletransmission +
                ", liaisons=" + liaisons +
                '}';
    }
}
