package fr.gmv.vetosphere.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "liaison")
public class Liaison extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "auth_key", nullable = false, unique = true, length = 255)
    private String authKey;

    @Column(name = "site_id")
    private Long siteId;

    @Column(name = "isDeleted")
    private Boolean isDeleted;

    @OneToMany(mappedBy = "liaison", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonIgnore
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Teletransmission> teletransmissions;

    @ManyToOne
    @JoinColumn(name = "assurance_id", nullable = false)
    private Assurance assurance;

    public Liaison() {
    }

    public Liaison(String authKey, Long siteId, Boolean isDeleted) {
        this.authKey = authKey;
        this.siteId = siteId;
        this.isDeleted = isDeleted;
    }

    public Liaison(String authKey, Long siteId, Boolean isDeleted,
                   Set<Teletransmission> teletransmissions, Assurance assurance) {
        this.authKey = authKey;
        this.siteId = siteId;
        this.isDeleted = isDeleted;
        this.teletransmissions = teletransmissions;
        this.assurance = assurance;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Set<Teletransmission> getTeletransmissions() {
        return teletransmissions;
    }

    public void setTeletransmissions(Set<Teletransmission> teletransmissions) {
        this.teletransmissions = teletransmissions;
    }

    public Assurance getAssurance() {
        return assurance;
    }

    public void setAssurance(Assurance assurance) {
        this.assurance = assurance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Liaison liaison = (Liaison) o;
        return Objects.equals(id, liaison.id) &&
                Objects.equals(authKey, liaison.authKey) &&
                siteId.equals(liaison.siteId) &&
                Objects.equals(isDeleted, liaison.isDeleted) &&
                Objects.equals(teletransmissions, liaison.teletransmissions) &&
                Objects.equals(assurance, liaison.assurance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, authKey, siteId, isDeleted, teletransmissions, assurance);
    }

    @Override
    public String toString() {
        return "Liaison{" +
                "id=" + id +
                ", authKey='" + authKey + '\'' +
                ", siteId=" + siteId +
                ", isDeleted=" + isDeleted +
                ", teletransmissions=" + teletransmissions +
                ", assurance=" + assurance +
                '}';
    }
}
