package fr.gmv.vetosphere.repository;

import fr.gmv.vetosphere.domain.Teletransmission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeletransmissionRepository extends JpaRepository<Teletransmission, Long> {
}
