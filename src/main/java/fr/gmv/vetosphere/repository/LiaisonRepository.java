package fr.gmv.vetosphere.repository;

import fr.gmv.vetosphere.domain.Liaison;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LiaisonRepository extends JpaRepository<Liaison, Long> {
}
