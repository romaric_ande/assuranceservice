package fr.gmv.vetosphere.repository;

import fr.gmv.vetosphere.domain.Assurance;
import fr.gmv.vetosphere.service.dto.AssuranceDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface AssuranceRepository extends JpaRepository<Assurance, Long>, QuerydslPredicateExecutor<Assurance> {

    /**
     * Finds Assurance by using id as a search criteria.
     *
     * @param id
     * @return An Object Assurance whose id is equals to the given id. If
     *         no Assurance is found, this method returns null.
     */
    /*@Query("select e from Assurance e where e.id = :id and e.isDeleted = :isDeleted")
    Assurance findById(@Param("id") Long id, @Param("isDeleted")Boolean isDeleted);

    @Query("select e from Assurance e where e.id = :id and e.isDeleted = :isDeleted")
    List<Assurance> findAssuranceById(@Param("id")Long id, @Param("isDeleted")Boolean isDeleted);

    @Query("select e from Assurance e where e.name = :name")
    List<Assurance> findByName(@Param("name")String name);*/

    /*@Query("select e from Assurance e where e.isDeleted = true")
    List<Assurance> getAllAssurance();*/

    List<Assurance> findAssuranceById(Long id);

    @Query(value = "SELECT id, name, prix, prixAchat FROM Assurance p WHERE p.price = :prixLimit", nativeQuery = true)
    List<Assurance> chercherProduitPrix(@Param("prixLimit") int prix);

    @Query(value = "SELECT * FROM Assurance", nativeQuery = true)
    List<Assurance> getAllAssurance();

    @Modifying
    @Query("DELETE FROM Assurance a WHERE a.id = :id")
    Integer deleteByAccountId(@Param("id")Long id);
}
