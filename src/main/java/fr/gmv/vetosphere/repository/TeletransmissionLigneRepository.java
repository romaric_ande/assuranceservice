package fr.gmv.vetosphere.repository;

import fr.gmv.vetosphere.domain.TeletransmissionLigne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeletransmissionLigneRepository extends JpaRepository<TeletransmissionLigne, Long> {
}
