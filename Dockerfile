FROM adoptopenjdk/openjdk11

ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    SPRING_BOOT_SLEEP=0 \
    JAVA_OPTS=""

# add directly the war
ARG JAR_FILE
ADD target/${JAR_FILE} /app.war

EXPOSE 8080
CMD echo "The application will start in ${SPRING_BOOT_SLEEP}s..." && \
    sleep ${SPRING_BOOT_SLEEP} && \
    java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar /app.war


